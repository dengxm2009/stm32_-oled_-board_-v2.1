# STM32_OLED_BOARD_V2.1

#### 介绍
CAN通信和485通信和UART通信 通过0.78寸OLED显示屏多级菜单显示关键信息。

#### 软件架构
通过CubeMX配置软件初始化状态。oeld通过IIC接口驱动。


#### 硬件介绍

![原理图 主控芯片](https://images.gitee.com/uploads/images/2021/0305/160705_e6513f65_5440508.png "屏幕截图.png")
![原理图 电源部分](https://images.gitee.com/uploads/images/2021/0305/160809_b07d6313_5440508.png "屏幕截图.png")
![原理图 OLED显示](https://images.gitee.com/uploads/images/2021/0305/160847_371f74b0_5440508.png "屏幕截图.png")
![PCB 3d显示](https://images.gitee.com/uploads/images/2021/0305/160203_bf753899_5440508.png "屏幕截图.png")


1.  串口转USB
2.  通讯接口：CAN 485 UART SPI
3.  IO

#### 多级菜单控制

1.  通过多级菜单显示
2.  打印不同界面信息
3.  任务管理

#### 相关连接

1.  OLED多级菜单显示教程 https://blog.csdn.net/weixin_42618564?spm=1000.2115.3001.5343


