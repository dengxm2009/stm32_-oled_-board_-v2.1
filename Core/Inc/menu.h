#ifndef __MENU_H
#define __MENU_H

//私有定义
void Menu_key_set(void);
void fun0(void);
void fun1(void);
void fun2(void);
void fun3(void);
void fun4(void);
void fun5(void);
void fun6(void);
void fun7(void);
void fun8(void);
void fun9(void);
void fun10(void);
void fun11(void);
void fun12(void);
void fun13(void);
void fun14(void);
void fun15(void);
void fun16(void);
void fun17(void);
void fun18(void);
void fun19(void);
void fun20(void);
void fun21(void);
void fun22(void);
void fun23(void);
void fun24(void);
void fun25(void);

typedef struct
{
    uint8_t current;//当前状态索引号
    uint8_t next;//向下按
    uint8_t enter;//确定键按
    void (*current_operation)(void);//当前状态应该执行的操作
}Menu_table;

extern uint32_t TxMailbox;
extern uint8_t Data_current[8];
extern CAN_TxHeaderTypeDef TxHeader;
extern CAN_RxHeaderTypeDef RxHeader;
#endif /* __MENU_H */
